//
//  Item.swift
//  Todoey
//
//  Created by Warbi Võ  on 14/03/2022.
//  Copyright © 2022 Warbi Võ. All rights reserved.
//

import Foundation
import RealmSwift

class Item : Object {
    @objc dynamic var title: String = ""
    @objc dynamic var done: Bool = false
    @objc dynamic var dateCreated: Date?
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
