//
//  Category.swift
//  Todoey
//
//  Created by Warbi Võ  on 14/03/2022.
//  Copyright © 2022 Warbi Võ. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var colour: String = ""
    let items = List<Item>()
    
}
	
